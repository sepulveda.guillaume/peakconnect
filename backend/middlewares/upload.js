const multer = require("multer");

const DIR = "./pictures/";

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, DIR);
  },
  filename: (req, file, callback) => {
    const { originalname } = file;
    const nameWithoutExtension = originalname.split(".").slice(0, -1).join(".");
    const extension = originalname.split(".").pop();
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
    const newFilename = `${nameWithoutExtension}-${uniqueSuffix}.${extension}`;

    callback(null, newFilename);
  },
});

var upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/jpeg"
    ) {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
    }
  },
});

module.exports = upload.single("picture");

const express = require('express');
const app = express();
const port = process.env.PORT || 3000
const cors = require('cors');
const mongoose = require('mongoose');
const router = require('./routes/index');
const path = require('path');

require('dotenv').config();

mongoose.connect(process.env.MANGOOSE_CONNECTION)
.then(() => console.log('Connexion à MongoDB réussie !'))
.catch(() => console.log('Connexion à MongoDB échouée !'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

// Serve static files from the 'pictures' directory
app.use('/pictures', express.static(path.join(__dirname, 'pictures')));

app.use('/', router);

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});

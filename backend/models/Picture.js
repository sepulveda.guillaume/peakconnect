const mongoose = require("mongoose");

const pictureSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: false,
      maxLength: 100,
    },
    description: {
      type: String,
      required: false,
      maxLength: 300,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: false,
    },
    picturePath: {
      type: String,
      required: false,
    },
    uploadDate: { 
      type: Date, 
      default: Date.now 
    },
    views: {
      type: Number,
      default: 0
    },
  },
  {
    timestamps: true,
  }
);

const Picture = mongoose.model("Picture", pictureSchema);

module.exports = Picture;

const router = require("express").Router();
const pictureRoutes = require("./picture.routes");
const userRoutes = require("./user.routes");

router.use("/pictures", pictureRoutes);
router.use("/users", userRoutes);

module.exports = router;

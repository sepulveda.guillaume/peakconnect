const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');

// Signup user
router.post('/signup', userController.signup);

// Login user
router.post('/signin', userController.signin);

// GET users
router.get('/', userController.getAllUsers);

// GET users/:id
router.get('/:id', userController.getUserById);

// POST users
router.post('/', userController.createUser);

// PUT users/:id
router.put('/:id', userController.updateUser);

// DELETE users/:id
router.delete('/:id', userController.deleteUser);

module.exports = router;

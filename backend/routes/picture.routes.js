const express = require("express");
const router = express.Router();
const pictureController = require("../controllers/picture.controller");
// const { auth } = require("../middlewares/auth");
const upload = require("../middlewares/upload.js");

// GET pictures
router.get("/", pictureController.getAllPictures);

// GET pictures/:id
router.get("/:id", pictureController.getPictureById);

// POST pictures
router.post("/", upload, pictureController.createPicture);

// PUT pictures/:id
router.put("/:id", pictureController.updatePicture);

// DELETE pictures/:id
router.delete("/:id", pictureController.deletePicture);

module.exports = router;

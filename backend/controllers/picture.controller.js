const Picture = require("../models/Picture");
const User = require("../models/User");

const getAllPictures = async (req, res) => {
  try {
    const pictures = await Picture.find();
    res.json(pictures);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const getPictureById = async (req, res) => {
  try {
    const picture = await Picture.findById(req.params.id);
    if (!picture) {
      return res.status(404).json({ error: "Book not found" });
    }
    res.json(picture);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const createPicture = async (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).json({ error: "File not provided" });
    }

    const picturePath =
      req.protocol + "://" + req.get("host") + "/pictures/" + req.file.filename;

    const picture = await Picture.create({ ...req.body, picturePath });

    await User.findByIdAndUpdate(
      req.body.user,
      { $push: { pictures: picture._id } },
      { new: true }
    );

    res.status(201).json(picture);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const updatePicture = async (req, res) => {
  try {
    const picture = await Picture.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    if (!picture) {
      return res.status(404).json({ error: "Book not found" });
    }
    res.json(picture);
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

const deletePicture = async (req, res) => {
  try {
    const picture = await Picture.findByIdAndDelete(req.params.id);
    if (!picture) {
      return res.status(404).json({ error: "Book not found" });
    }
    res.json({ message: "Book deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal Server Error" });
  }
};

module.exports = {
  getAllPictures,
  getPictureById,
  createPicture,
  updatePicture,
  deletePicture,
};

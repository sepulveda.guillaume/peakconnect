export default function Button({ children, onClick }) {
  return (
    <button onClick={onClick} className="rounded-md bg-indigo-600 mx-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
      {children}
    </button>
  );
}

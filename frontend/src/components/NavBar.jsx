import { NavLink } from "react-router-dom";
import Button from "./Button";
import { useAuth } from "../hooks/useAuth";

export default function NavBar({ isLoggedIn }) {
  const { logout } = useAuth();

  const handleLogout = async () => {
    logout();
  };

  return (
    <nav className="flex items-center justify-end flex-wrap p-3">
      {isLoggedIn ? (
        <Button onClick={handleLogout}>
          <NavLink to="/" className="block px-3 py-1.5">
            Logout
          </NavLink>
        </Button>
      ) : (
        <>
          <Button>
            <NavLink to="/sign-up" className="block px-3 py-1.5">
              Sign Up
            </NavLink>
          </Button>
          <Button>
            <NavLink to="/sign-in" className="block px-3 py-1.5">
              Sign In
            </NavLink>
          </Button>
        </>
      )}
    </nav>
  );
}

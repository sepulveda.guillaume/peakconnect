import Picture from "./Picture";

export default function PictureList({ pictures, handleDelete }) {
  return (
    <div className="flex flex-col items-center justify-center">
      {pictures ? (
        pictures.map((picture) => (
          <Picture
            key={picture._id}
            picture={picture}
            handleDelete={handleDelete}
          />
        ))
      ) : (
        <p>Aucune photo trouvée</p>
      )}
    </div>
  );
}

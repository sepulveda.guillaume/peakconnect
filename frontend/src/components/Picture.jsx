import Button from "./Button";

export default function Picture({ picture, handleDelete }) {
  const onDelete = (id) => {
    handleDelete(id);
  };
  return (
    <div className="max-w-sm my-5 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      {picture.picturePath ? (
        <img
          className="rounded-t-lg"
          src={picture.picturePath}
          alt={picture.title}
        />
      ) : (
        <p>No image</p>
      )}
      <div className="p-5 flex flex-col items-center">
        {picture.title ? (
          <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            {picture.title}
          </h5>
        ) : (
          <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            No title
          </h5>
        )}
        {picture.user ? (
          <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
            {picture.user}
          </p>
        ) : (
          <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
            No user
          </p>
        )}
        <Button onClick={() => onDelete(picture._id)}>
          <div className="px-1.5 py-1.5">Delete</div>
        </Button>
      </div>
    </div>
  );
}

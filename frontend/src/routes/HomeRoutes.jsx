import { Outlet, Navigate } from "react-router-dom";
import { useAuth } from "../hooks/useAuth";

export default function HomeRoutes() {
  const { auth } = useAuth();

  return auth ? <Navigate to="/pictures" /> : <Outlet />;
}

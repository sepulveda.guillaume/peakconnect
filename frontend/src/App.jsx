import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Signup from "./pages/Signup";
import Signin from "./pages/Signin";
import PrivateRoutes from "./routes/PrivateRoutes";
import HomeRoutes from "./routes/HomeRoutes";
import Results from "./pages/Pictures";

export default function App() {
  return (
    <Routes>
      <Route element={<HomeRoutes />}>
        <Route element={<Home />} path="/" />
        <Route element={<Signup />} path="/sign-up" />
        <Route element={<Signin />} path="/sign-in" />
      </Route>
      <Route element={<PrivateRoutes />}>
        <Route element={<Results />} path="/pictures" />
      </Route>
    </Routes>
  );
}

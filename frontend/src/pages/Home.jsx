import NavBar from "../components/NavBar";
import { useEffect } from "react";

export default function Home() {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
        document.body.style.overflow = "scroll"
    };
  }, []);
  
  return (
    <>
      <NavBar isLoggedIn={false} />
      <div className="flex items-center justify-center h-screen">
        <h1 className="text-4xl">PeakConnect</h1>
      </div>
    </>
  );
}

import PictureList from "../components/PicturesList";
import useFetchData from "../hooks/useFetchData";
import NavBar from "../components/NavBar";
import Modal from "../components/Modal";
import { useState } from "react";
import Input from "../components/Input";

export default function Results() {
  const domain = import.meta.env.VITE_APP_DOMAIN;
  const {
    data: pictures,
    postData: handleCreate,
    deleteData: handleDelete,
  } = useFetchData(domain, "pictures");

  const [openModal, setOpenModal] = useState(false);
  const [picture, setPicture] = useState(null);
  const [pictureTitle, setPictureTitle] = useState("");
  const [error, setError] = useState(null);

  const handleSubmit = async () => {
    if (!picture && !pictureTitle) {
      setError("No file selected and no title provided");
    } else if (!picture) {
      setError("No file selected");
    } else if (!pictureTitle) {
      setError("No title provided");
    } else {
      const formData = new FormData();
      formData.append("picture", picture);
      formData.append("title", pictureTitle);

      await handleCreate(formData);
      setPicture(null);
      setPictureTitle("");
      setOpenModal(false);
    }
  };

  const modalContent = (
    <form>
      <Input
        label="Title"
        type="text"
        id="title"
        value={pictureTitle}
        onChange={(e) => setPictureTitle(e.target.value)}
      />
      <Input
        label="Picture"
        type="file"
        className="form-control"
        id="file"
        onChange={(e) => setPicture(e.target.files[0])}
      />
    </form>
  );

  return (
    <>
      <NavBar isLoggedIn={true} />
      <div className="flex min-h-full flex-1 flex-col justify-center px-6 lg:px-8 bg-gray-100">
        <div className="flex justify-center pt-5">
          <button
            onClick={() => setOpenModal(true)}
            className="flex items-center justify-center rounded-full w-10 h-10 bg-indigo-600 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z"
              />
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z"
              />
            </svg>
          </button>
        </div>
        {openModal && (
          <Modal
            openModal={openModal}
            setOpenModal={setOpenModal}
            title="Add a new story"
            content={modalContent}
            handleSubmit={() => handleSubmit({ picture, title: pictureTitle })}
            error={error}
          />
        )}
        <PictureList pictures={pictures} handleDelete={handleDelete} />
      </div>
    </>
  );
}

import { createContext, useContext } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "./useLocalStorage";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const domain = import.meta.env.VITE_APP_DOMAIN;

  const [auth, setAuth] = useLocalStorage("auth", null);
  const navigate = useNavigate();

  const signup = async (email, password) => {
    try {
      const formData = new FormData();
      formData.append("email", email);
      formData.append("password", password);

      await axios.post(`${domain}/users/signup`, formData, {
        headers: {
          "Content-Type": "application/json",
        },
      });
    } catch (error) {
      console.error("Error:", error);
    }
  }

  const signin = async (email, password) => {
    try {
      const formData = new FormData();
      formData.append("email", email);
      formData.append("password", password);

      const response = await axios.post(`${domain}/users/signin`, formData, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      setAuth({ token: response.data.token });
      navigate("/pictures");
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const logout = () => {
    setAuth(null);
    localStorage.removeItem("auth");
    navigate("/");
  };

  return (
    <AuthContext.Provider value={{ auth, signup, signin, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};

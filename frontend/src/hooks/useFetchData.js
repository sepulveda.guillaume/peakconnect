import { useState, useEffect } from "react";
import axios from "axios";

export default function useFetchData(domain, apiPath) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${domain}/${apiPath}`);
        setData(response.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const postData = async (formData) => {
    try {
      const response = await axios.post(`${domain}/${apiPath}`, formData);
      setData((prevData) => [...prevData, response.data]);
      console.log("Uploaded successfully");
    } catch (error) {
      setError(error);
    }
  };

  const putData = async (id, newData) => {
    try {
      const response = await axios.put(`${domain}/${apiPath}/${id}`, newData);
      setData((prevData) =>
        prevData.map((item) => (item._id === id ? response.data : item))
      );
    } catch (error) {
      setError(error);
    }
  };

  const deleteData = async (id) => {
    try {
      await axios.delete(`${domain}/${apiPath}/${id}`);
      setData((prevData) => prevData.filter((item) => item._id !== id));
    } catch (error) {
      setError(error);
    }
  };

  return { data, loading, error, postData, putData, deleteData };
}
